package com.example.Session41MongoDBSyehAKobarSpring.controller;

import com.example.Session41MongoDBSyehAKobarSpring.exception.DataNotFoundException;
import com.example.Session41MongoDBSyehAKobarSpring.model.Barang;
import com.example.Session41MongoDBSyehAKobarSpring.model.Inventori;
import com.example.Session41MongoDBSyehAKobarSpring.repository.BarangRepo;
import com.example.Session41MongoDBSyehAKobarSpring.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BarangController {
    @Autowired
    private BarangRepo barangRepo;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @GetMapping("/semua")
    public List<Barang> getAll(){
        List<Barang> barangList = barangRepo.findAll();
        return barangList;
    }
    @GetMapping("/{nama}")
    public List<Barang> getNama(@PathVariable("nama") String nama){
        List<Barang> barangList= barangRepo.findByNama(nama);
        return barangList;
    }
    @GetMapping("/{id}")
    public Barang getId(@PathVariable String id){
        Barang barang= barangRepo.findById(id)
                .orElseThrow(()-> new DataNotFoundException(id));
        return barang;
    }
    @GetMapping("/inventori")
    public List<Inventori> getInventori(){
        List<Inventori> inventoriList = new ArrayList<>();
        List<Barang> barangList = barangRepo.findAll();
        for(Barang barang: barangList){
            Inventori inventori = modelMapperUtil
                    .ModelMapperInit()
                    .map(barang, Inventori.class);
            inventori.setId(barang.getId());
            inventori.setNama(barang.getNama());
            inventoriList.add(inventori);
        }
        return inventoriList;
    }
    @PostMapping("/baru")
    public Barang newBarang(@RequestBody Barang barang){
        return barangRepo.save(barang);
    }

}
