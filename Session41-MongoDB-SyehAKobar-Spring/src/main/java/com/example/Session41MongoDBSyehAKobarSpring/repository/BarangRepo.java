package com.example.Session41MongoDBSyehAKobarSpring.repository;

import com.example.Session41MongoDBSyehAKobarSpring.model.Barang;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "barang",path = "barang")
public interface BarangRepo extends MongoRepository<Barang,String> {
    List<Barang> findByNama(@Param("nama") String nama);
}
