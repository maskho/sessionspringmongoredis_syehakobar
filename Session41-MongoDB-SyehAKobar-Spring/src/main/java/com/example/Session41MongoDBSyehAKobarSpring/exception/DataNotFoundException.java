package com.example.Session41MongoDBSyehAKobarSpring.exception;

public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException(String id){
        super("Data dengan id: "+id+" tidak ada di database");
    }

}
