package com.example.Session41MongoDBSyehAKobarSpring.util;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperUtil {
    public ModelMapper ModelMapperInit(){
        return new ModelMapper();
    }

}
