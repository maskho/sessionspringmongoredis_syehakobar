package com.example.Session41MongoDBSyehAKobarSpring.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Barang {
    @Id private String id;

    private String nama;
    private int harga;
    private Stok stok;
}
