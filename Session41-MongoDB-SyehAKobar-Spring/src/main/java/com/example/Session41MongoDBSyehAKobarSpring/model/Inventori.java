package com.example.Session41MongoDBSyehAKobarSpring.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Inventori {
    private String id;
    private String nama;
}
