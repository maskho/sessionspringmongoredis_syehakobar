package com.example.Session41MongoDBSyehAKobarSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Session41MongoDbSyehAKobarSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Session41MongoDbSyehAKobarSpringApplication.class, args);
	}

}
